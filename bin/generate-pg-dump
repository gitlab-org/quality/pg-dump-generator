#!/usr/bin/env ruby

require 'optimist'
require_relative '../lib/pg_dump_generator'
require_relative '../lib/slack'

opts = Optimist.options do
  banner "Options:"
  opt :gitlab_version, "The GitLab version to generate PG dump for. Must be in the format Major.Minor.Patch like 17.1.3", type: :string
  opt :gitlab_docker_image, "GitLab docker image to use for generation.", type: :string, default: "gitlab/gitlab-ee"
  opt :gds_config_name, "GitLab Data Seeder configuration name.", type: :string, default: "bulk_data.rb"
  opt :gitlab_repo_path, "Full Path to the local GitLab repository to be mounted to GitLab container.", type: :string
  opt :seed_fu, "Seed GitLab docker image using Seed Fu and GitLab fixtures.", type: :flag, default: true
  opt :force, "Force PG dump generation even if files exist.", type: :flag, default: false
  opt :commit, "Commit and merge PG dump after generation. Requires 'ACCESS_TOKEN' environment variable.", type: :flag, default: false
  opt :channel, "Slack channel to post results to.", type: :string, default: "tp-upgrade-results"
  opt :report_slack, "Report results to Slack channel. Requires 'SLACK_BOT_TOKEN' environment variable.", type: :flag, default: false
  banner "Generate PG dump for the latest upgrade stop:"
  banner "    #{$PROGRAM_NAME}"
  banner "Generate PG dump for the latest upgrade stop using local GDK path:"
  banner "    #{$PROGRAM_NAME} --gitlab-repo-path '/Users/username/gitlab-development-kit/gitlab'"
  banner "Generate PG dump for the specific GitLab version and Docker image:"
  banner "    #{$PROGRAM_NAME} --gitlab-version 16.3.6 --gitlab-docker-image gitlab/gitlab-ce"
end

begin
  pg_dump_generator = PGDumpGenerator.new(opts)

  pg_dump_file_path = pg_dump_generator.create_pg_dump_gz_files
  mr_web_url = pg_dump_generator.commit_and_merge_changes(pg_dump_file_path) if opts[:commit] && ENV['ACCESS_TOKEN']

  Slack.post_results(channel: opts[:channel], passed: true, pg_filename: pg_dump_file_path, mr_web_url:) if opts[:report_slack]
rescue PGDumpGenerator::PGDumpExistsError => e
  puts e
  exit 0
rescue Interrupt
  puts "Caught the interrupt. Stopping."
  exit 130
rescue Exception => e # rubocop:disable Lint/RescueException -- catching all possible exceptions
  puts "\nPG dump generation failed:\n#{e.class} - #{e.exception}\n Traceback:#{e.backtrace}"
  Slack.post_results(channel: opts[:channel], passed: false) if opts[:report_slack]
  exit 1
end
