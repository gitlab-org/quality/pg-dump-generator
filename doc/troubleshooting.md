# Troubleshooting

General troubleshooting steps when PG Dump creation job fails:

1. Check job failure: what GitLab version and command options were used, where pipeline failed
1. Get familiar with [PG Dump Generator workflow](../README.md#overview)
1. Follow [Running the PG Dump Generator](../README.md#running-the-pg-dump-generator) to run the command locally
1. If Failure happens during `gitlab-rake "ee:gitlab:seed:data_seeder[bulk_data.rb]"`:
    - Check if `bulk_data.rb` file was updated recently that might have caused it
    - If not, debug `data_seeder` rake command against GDK or Docker image following `Running the PG Dump Generator`

## `PG::UniqueViolation: ERROR:  duplicate key value violates unique constraint "index_ci_namespace_mirrors_on_namespace_id"`

- Example - `https://gitlab.com/gitlab-org/quality/pg-dump-generator/-/jobs/6286710638#L1302`

Known flaky issue with Factories, should pass on retry, see context at [issues/2354#note_1793812916](https://gitlab.com/gitlab-org/quality/quality-engineering/team-tasks/-/issues/2354#note_1793812916).

## PG dump file is smaller than expected

PG dump file should be bigger than 500 Kb after seeding. Empty database export is usually around 300Kb and seeded PG file is expected to be larger than 500kb, around 1Mb.
If PG dump size is smaller, it requires further investigation if all seeding options worked as expected. Navigate to specific job that created the PG dump and ensure that both Data Seeder and `db/fixtures` were seeded correctly.
