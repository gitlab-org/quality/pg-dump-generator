# frozen_string_literal: true

module Slack
  extend self

  SLACK_BOT_TOKEN = ENV['SLACK_BOT_TOKEN']
  TROUBLESHOOTING_GUIDE = 'https://gitlab.com/gitlab-org/quality/pg-dump-generator/-/blob/main/doc/troubleshooting.md'

  def post_slack_message(channel, message)
    url = "https://slack.com/api/chat.postMessage"

    body = {}
    body['token'] = SLACK_BOT_TOKEN
    body['channel'] = channel
    body['text'] = message

    response = HTTParty.post(url, body:)
    puts "POST request Slack message failed!" unless response.ok?
  end

  def post_results(channel:, passed:, pg_filename: nil, mr_web_url: nil)
    if SLACK_BOT_TOKEN.nil?
      puts "No 'SLACK_BOT_TOKEN' environment variable is provided for #{channel}"
      return
    end

    initial_msg = "PG dump generation pipeline"
    message = passed ? ":ci_passing: #{initial_msg} passed! :ci_passing:" : ":ci_failing: #{initial_msg} failed! :ci_failing:"

    if pg_filename
      pg_file_size_kb = (File.size(pg_filename).to_f / 1024).round(2)
      message << "\n\n• PG dump file name - `#{pg_filename}`, size: #{pg_file_size_kb} Kb"
      message << "\n\n• :warning: PG dump file is smaller than expected, check <#{TROUBLESHOOTING_GUIDE}|Toubleshooting guide>" if pg_file_size_kb < 500
    end

    message << "\n\n• PG dump file was added with merge request #{mr_web_url}" if mr_web_url
    message << "\n\n• <#{ENV['CI_PIPELINE_URL']}|Pipeline>" if ENV['CI_PIPELINE_URL']
    message << "\n\n• <#{TROUBLESHOOTING_GUIDE}|Toubleshooting guide>" unless passed

    puts "Sending Slack report to channel #{channel}..."
    post_slack_message(channel, message)
  end
end
