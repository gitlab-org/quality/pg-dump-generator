# frozen_string_literal: true

require 'gitlab'
require 'fileutils'
require 'yaml'
require 'semantic'
require 'open3'
require 'base64'

class PGDumpGenerator
  PGDUMP_PROJECT_ID = '55016281'
  API_URL = 'https://gitlab.com/api/v4'
  TOKEN = ENV['ACCESS_TOKEN']
  UPGRADE_TOOL_PATH = 'https://gitlab.com/gitlab-org/gitlab/-/raw/master/config/upgrade_path.yml'
  OMNIBUS_PROJECT_ID = '20699'
  LATEST_UPGRADE_STOP_FILENAME = 'latest_upgrade_stop.gz'
  MR_LABELS = 'type::maintenance,maintenance::pipelines,automation:bot-authored'

  GITLAB_PROJECT_CLONE_BRANCH = ENV['GITLAB_PROJECT_CLONE_BRANCH']
  GITLAB_DOCKER_NO_TEARDOWN = ENV['GITLAB_DOCKER_NO_TEARDOWN']
  PDG_SKIP_MERGING = ENV['PDG_SKIP_MERGING']
  PDG_POST_SEED_PSQL_QUERY = ENV['PDG_POST_SEED_PSQL_QUERY']

  PGDumpExistsError = Class.new(StandardError)

  def initialize(opts)
    @use_latest_upgrade_stop = opts[:gitlab_version].nil?
    @gitlab_version = @use_latest_upgrade_stop ? latest_upgrade_stop_version : Semantic::Version.new(opts[:gitlab_version])
    @gitlab_docker_image = opts[:gitlab_docker_image]
    @gitlab_edition = @gitlab_docker_image.include?("ee") ? "ee" : "ce"
    @gds_config_name = opts[:gds_config_name]
    @tmp_dir = Pathname.new(File.expand_path("../tmp", __dir__))
    @pg_dumps_folder = Pathname.new(File.expand_path("../pg_dumps/#{@gitlab_edition}", __dir__))
    @gitlab_local_repo_path = opts[:gitlab_repo_path].nil? ? "#{@tmp_dir}/gitlab" : opts[:gitlab_repo_path]

    @force = opts[:force]
    @seed_fu = opts[:seed_fu]
  end

  def create_pg_dump_gz_files
    pg_dump_file_path = "#{@pg_dumps_folder}/#{@gitlab_version}.gz"

    raise PGDumpExistsError, "PG dump already exists at #{pg_dump_file_path}.\nUse `--force` option to overwrite existing file." if File.file?(pg_dump_file_path) && !@force

    unless File.directory?(@gitlab_local_repo_path)
      puts "Preparing lastest GitLab repo clone at #{@gitlab_local_repo_path}"
      git_clone_gitlab(@gitlab_local_repo_path)
    end

    puts "Outputting HEAD SHA of the GitLab path..."
    run_command("cd #{@gitlab_local_repo_path};git rev-parse HEAD")

    puts "Starting GitLab Docker container and mounting '#{@gitlab_local_repo_path}' to it..."
    pg_dump_archive_path = create_pg_dump(@gitlab_local_repo_path)

    puts "Copying PG dump to #{pg_dump_file_path}.\nUpdating #{LATEST_UPGRADE_STOP_FILENAME} file: #{@use_latest_upgrade_stop}"
    FileUtils.mkdir_p(@pg_dumps_folder) unless File.directory?(@pg_dumps_folder)
    FileUtils.cp(pg_dump_archive_path, pg_dump_file_path)
    FileUtils.cp(pg_dump_archive_path, "#{@pg_dumps_folder}/#{LATEST_UPGRADE_STOP_FILENAME}") if @use_latest_upgrade_stop

    remove_container
    pg_dump_file_path
  end

  # Required: Environment variable ACCESS_TOKEN=<gitlab personal access API token>
  #
  def commit_and_merge_changes(pg_dump_file_path)
    @gl_client = Gitlab.client(
      endpoint: API_URL,
      private_token: TOKEN
    )

    new_branch_name = "#{Time.now.strftime('%Y-%m-%d_%Hh-%Mm')}_pg-dump-update-#{@gitlab_version}"
    puts "Creating new branch - #{new_branch_name}..."
    @gl_client.create_branch(PGDUMP_PROJECT_ID, new_branch_name, 'main')

    puts "Creating new file - #{pg_dump_file_path}..."
    file_path = Pathname.new(pg_dump_file_path).relative_path_from(Dir.pwd).to_s
    commit_file_to_repository(PGDUMP_PROJECT_ID, file_path, File.read(pg_dump_file_path), new_branch_name, "Add PG dump for GitLab v#{@gitlab_version}")

    if @use_latest_upgrade_stop
      latest_pg_dump_file_path = "#{@pg_dumps_folder}/#{LATEST_UPGRADE_STOP_FILENAME}"
      file_path = Pathname.new(latest_pg_dump_file_path).relative_path_from(Dir.pwd).to_s
      puts "Updating latest upgrade stop file - #{file_path}..."
      commit_file_to_repository(PGDUMP_PROJECT_ID, file_path, File.read(latest_pg_dump_file_path), new_branch_name, "Update latest PG dump for GitLab v#{@gitlab_version}")
    end

    mr_title = "[#{Date.today}] Add PG dump for GitLab version #{@gitlab_version}"
    description = "Adding PG dump file for #{@gitlab_version}.\nThis MR has been generated from pipeline #{ENV['CI_PIPELINE_URL']}."
    mr_options = {
      description:,
      source_branch: new_branch_name,
      target_branch: 'main',
      remove_source_branch: true,
      squash: true,
      labels: MR_LABELS
    }
    mr = @gl_client.create_merge_request(PGDUMP_PROJECT_ID, mr_title, mr_options)

    return if PDG_SKIP_MERGING

    sleep 60 # Give the MR some time to be ready
    @gl_client.accept_merge_request(PGDUMP_PROJECT_ID, mr["iid"].to_s)
    mr["web_url"]
  end

  private

  ## Determining upgrade stop version

  def get_latest_patch(ver)
    response = HTTParty.get("https://gitlab.com/api/v4/projects/#{OMNIBUS_PROJECT_ID}/repository/tags?search=^#{ver}.").body
    # Removing release candidate tags to get released stable patch
    gitlab_versions = JSON.parse(response, symbolize_names: true)
    return if gitlab_versions.empty?

    gitlab_versions.delete_if { |version| version[:name].include?("rc") }
    return if gitlab_versions.empty?

    Semantic::Version.new(gitlab_versions[0][:name].match(/(\d+\.\d+\.\d+)/)[1])
  end

  def latest_upgrade_stop_version
    puts "Determining latest upgrade stop version from #{UPGRADE_TOOL_PATH}..."
    upgrade_path_yml = HTTParty.get(UPGRADE_TOOL_PATH).body
    full_upgrade_path = YAML.safe_load(upgrade_path_yml, symbolize_names: true)
    last_upgrade_stop = latest_released_upgrade_stop_version(full_upgrade_path)

    raise "Latest released upgrade stop not found." if last_upgrade_stop.nil?

    puts "Latest upgrade stop version is #{last_upgrade_stop}"
    last_upgrade_stop
  end

  # Stops in upgrade path are being added before they're released
  def latest_released_upgrade_stop_version(full_upgrade_path)
    full_upgrade_path.to_enum.with_index.reverse_each do |upgrade_stop, i|
      current_version = "#{upgrade_stop[:major]}.#{upgrade_stop[:minor]}"
      latest_patch = get_latest_patch(current_version)
      return latest_patch if latest_patch

      previous_version = "#{full_upgrade_path[i - 1][:major]}.#{full_upgrade_path[i - 1][:minor]}"
      puts "GitLab release is not avaialble for #{current_version}. Switching to the version before the last upgrade stop version - #{previous_version}"
    end
  end

  ## Common

  def run_command(cmd, raise_on_error: true)
    status = nil
    cmd_status = nil

    puts "Running command: '#{cmd}'"

    Open3.popen2e("#{cmd} 2>&1; echo $?") do |stdin, stdout_stderr, wait_thr|
      stdin.close
      output = stdout_stderr.readlines
      # cmd_status - workaround for catching errors from commands run with Docker exec
      # If Docker exec can find the container and run the command, it returns a success status, even if the command it runs inside the container fails.
      cmd_status = output.last.scan(/\d+/)[0].to_i # Get the last line of the output, which is the exit status of docker exec command
      output.each { |line| puts line.lstrip } # Print the rest of the output
      status = wait_thr.value
    end

    if status.success? && cmd_status.zero?
      true
    else
      error_msg = "'#{cmd}' failed, exit status=#{cmd_status}..."
      puts error_msg
      raise if raise_on_error

      false
    end
  end

  ### Prepare GitLab repo

  def git_clone_gitlab(gitlab_repo_path)
    gitlab_http_repo = "https://gitlab.com/gitlab-org/gitlab.git"
    cmd_options = "--depth=1"
    cmd_options += " --branch #{GITLAB_PROJECT_CLONE_BRANCH}" unless GITLAB_PROJECT_CLONE_BRANCH.nil?
    clone_cmd = "git clone #{cmd_options} #{gitlab_http_repo} #{gitlab_repo_path}"
    run_command(clone_cmd)
  end

  ## PG dump with Docker

  def create_pg_dump(gitlab_local_repo_path)
    gitlab_image = pull_docker_image
    start_gitlab_container(gitlab_image, gitlab_local_repo_path)

    puts "Waiting for GitLab Container to be ready..."
    wait_until_gitlab_healthy
    prepare_gds_configuration
    run_gds
    seed_fu_db_fixtures if @seed_fu
    run_custom_sql
    create_pg_dump_gz
    copy_pg_dump_from_docker
  end

  def pull_docker_image
    image_tag = "#{@gitlab_version}-#{@gitlab_edition}.0"
    gitlab_image_full_path = "#{@gitlab_docker_image}:#{image_tag}"
    run_command("docker pull #{gitlab_image_full_path}", raise_on_error: true)
    gitlab_image_full_path
  end

  def start_gitlab_container(image, gitlab_tmp_folder)
    @gitlab_cntr_name = "pgdump_#{@gitlab_version}"
    cmd_options = <<~DOC
      -d \
      -v #{gitlab_tmp_folder}/scripts/data_seeder:/opt/gitlab/embedded/service/gitlab-rails/scripts/data_seeder \
      -v #{gitlab_tmp_folder}/ee/db/seeds/data_seeder:/opt/gitlab/embedded/service/gitlab-rails/ee/db/seeds/data_seeder \
      -v #{gitlab_tmp_folder}/ee/lib/tasks/gitlab/seed:/opt/gitlab/embedded/service/gitlab-rails/ee/lib/tasks/gitlab/seed \
      --name #{@gitlab_cntr_name} \
      #{image}
    DOC
    run_command("docker run #{cmd_options}")
  end

  def wait_until_gitlab_healthy
    retries ||= 0
    sleep 10
    docker_healthcheck = Open3.capture2e("docker inspect -f {{.State.Health.Status}} #{@gitlab_cntr_name}")[0].strip == 'healthy'

    raise "Docker healthcheck failed #{docker_healthcheck}" unless docker_healthcheck
  rescue RuntimeError => e
    sleep 5
    puts "."
    retries += 1
    if retries > 120
      puts Open3.capture2e("docker logs #{@gitlab_cntr_name}")[0].strip
      raise e
    end

    retry
  rescue Interrupt
    puts "Caught the interrupt. Stopping and removing Docker container."
    remove_container
    exit
  end

  def remove_container
    puts "Removing container #{@gitlab_cntr_name}..."
    run_command("docker rm -f #{@gitlab_cntr_name}") unless GITLAB_DOCKER_NO_TEARDOWN
  end

  # GitLab Data Seeder

  def prepare_gds_configuration
    latest_upgrade_stop_tag = "v#{@gitlab_version}-#{@gitlab_edition}"
    run_command("docker exec #{@gitlab_cntr_name} bash -c \"cd /opt/gitlab/embedded/service/gitlab-rails; REF='#{latest_upgrade_stop_tag}' . scripts/data_seeder/test_resources.sh\"")
    run_command("docker exec #{@gitlab_cntr_name} bash -c \"cd /opt/gitlab/embedded/service/gitlab-rails; echo \\\"gem 'gitlab-rspec', path: 'gems/gitlab-rspec'\\\" >> Gemfile\"")
    run_command("docker exec #{@gitlab_cntr_name} bash -c \"cd /opt/gitlab/embedded/service/gitlab-rails; ruby scripts/data_seeder/globalize_gems.rb; bundle install\"")
    run_command("docker exec #{@gitlab_cntr_name} bash -c \"gitlab-ctl reconfigure\"")
  end

  def run_gds
    retries ||= 0
    seeding_status = run_command("docker exec #{@gitlab_cntr_name} gitlab-rake \"ee:gitlab:seed:data_seeder[#{@gds_config_name}]\"", raise_on_error: false)

    raise "GitLab Data Seeder command failed. See output for debugging." unless seeding_status
  rescue RuntimeError => e
    puts "Attempt ##{retries}"
    retries += 1
    if retries > 5
      puts "GitLab Data Seeder command failed."
      remove_container
      raise e
    end

    retry
  end

  # Running eaching seed file one by one to ensure
  # that seeding continues even if one seed file fails.
  # Seeding fixtures should only use fixture code from the relevant required stop ref
  # because fixtures are updated with GitLab code. If using code from lastest `master`
  # there is a risk of seeding with fileds that don't exist yet in required stop.
  def seed_fu_db_fixtures
    fixture_files = Dir.glob("#{@gitlab_local_repo_path}/db/fixtures/{development,production}/*.rb")

    fixture_files.each do |fixture_file|
      fixture_name = File.basename(fixture_file)
      fixture_folder = File.dirname(fixture_file).split('/').last # development or production
      mass_insert_env_var = "-e MASS_INSERT=1 -e CI=1" # use mass insert but smaller scale as in CI used in 'gitlab'
      db_fixture_env_var = "#{mass_insert_env_var} -e FILTER=#{fixture_name} -e FIXTURE_PATH=/opt/gitlab/embedded/service/gitlab-rails/db/fixtures/#{fixture_folder}"
      seeding_status = run_command("docker exec #{db_fixture_env_var} #{@gitlab_cntr_name} gitlab-rake \"db:seed_fu\"", raise_on_error: false)

      unless seeding_status
        puts "GitLab SeedFu command failed. See output for debugging."
        next
      end
    end
  end

  def run_custom_sql
    return if PDG_POST_SEED_PSQL_QUERY.nil?

    # Could be used to clean up incorrect data generated from seeding
    # Context - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/167510#note_2136698048
    puts "Running custom SQL query #{PDG_POST_SEED_PSQL_QUERY} ..."
    run_command("docker exec #{@gitlab_cntr_name} gitlab-psql -c \"#{PDG_POST_SEED_PSQL_QUERY}\"")
  end

  def create_pg_dump_gz
    run_command("docker exec #{@gitlab_cntr_name} bash -c \"mkdir /tmp/xfer; chown gitlab-psql /tmp/xfer\"")
    run_command("docker exec --user gitlab-psql #{@gitlab_cntr_name} bash -c \"pg_dump -U gitlab-psql -h '/var/opt/gitlab/postgresql' gitlabhq_production | gzip > /tmp/xfer/gitlabhq_production.gz\"")
  end

  def copy_pg_dump_from_docker
    pg_dump_result_path = "#{@tmp_dir}/pg_dump"
    FileUtils.mkdir_p(pg_dump_result_path)
    run_command("docker cp #{@gitlab_cntr_name}:/tmp/xfer/gitlabhq_production.gz #{pg_dump_result_path}")
    "#{pg_dump_result_path}/gitlabhq_production.gz"
  end

  # GitLab gem `create_file` can't be used because its url_encode doesn't encode dots
  # which causes pg_dump.gz files not to be uploaded to correct file path
  # GitLab API requires dots to be encoded to `%2E`
  def commit_file_to_repository(project_id, file_path, content, branch, commit_message)
    # Use POST or PUT depending on the time latest upgrade PG dump is uploaded
    begin
      @gl_client.get_file(project_id, file_path, branch)
      http_method = 'put'
    rescue Gitlab::Error::NotFound
      puts "No existing #{file_path} found in #{branch}.\nSending POST request..."
      http_method = 'post'
    end

    encoded_content = Base64.strict_encode64(content)
    file_path_encoded = CGI.escape(file_path).gsub('.', '%2E')
    body = {
      branch:,
      commit_message:,
      content: encoded_content,
      encoding: 'base64'
    }

    response = HTTParty.method(http_method).call(
      "#{API_URL}/projects/#{project_id}/repository/files/#{file_path_encoded}",
      headers: { 'PRIVATE-TOKEN' => TOKEN },
      body:
    )

    raise "Failed to create file in the repository. Error: #{response.body}" unless response.success?

    puts "File successfully created in the repository."
  end
end
