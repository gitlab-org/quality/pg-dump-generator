require 'spec_helper'
require_relative '../lib/pg_dump_generator'

describe PGDumpGenerator do
  describe '#initialize' do
    context 'when gitlab_version is provided' do
      subject(:pg_dump_generator) { described_class.new(options) }

      let(:options) { { gitlab_version: '16.3.6', gitlab_docker_image: 'gitlab/gitlab-ee' } }

      it 'initializes with correct GitLab version' do
        expect(pg_dump_generator.instance_variable_get(:@gitlab_version)).to eq(Semantic::Version.new(options[:gitlab_version]))
      end
    end

    context 'when gitlab_version is not provided' do
      subject(:pg_dump_generator) { described_class.new(options) }

      let(:options) { { gitlab_version: nil, gitlab_docker_image: 'gitlab/gitlab-ee', gitlab_repo_path: nil } }

      let(:tags) do
        <<~JSON
          [
            {"message": "", "name": "16.7.6+rc42.ee.0"},
            {"message": "", "name": "16.7.5+ee.0"},
            {"message": "", "name": "16.7.5+ce.0"},
            {"message": "", "name": "16.7.4+ee.0"},
            {"message": "", "name": "16.7.4+ce.0"},
            {"message": "", "name": "16.7.3+ee.0"},
            {"message": "", "name": "16.7.3+ce.0"}
          ]
        JSON
      end

      let(:upgrade_path_yml) do
        <<~YML
          - major: 14
            minor: 10
          - major: 15
            minor: 0
          - major: 15
            minor: 4
            comments: "If you have multiple [web/rails nodes](https://docs.gitlab.com/ee/administration/reference_architectures/2k_users.html) upgrade to 15.1 first. See [15.2.0 Upgrade Notes](https://docs.gitlab.com/ee/update/#1520)"
          - major: 15
            minor: 11
          - major: 16
            minor: 1
            comments: "16.1 is a required step if you have [npm packages](https://docs.gitlab.com/ee/update/versions/gitlab_16_changes.html#1610)"
          - major: 16
            minor: 3
            comments: "16.0 is a required upgrade step for [instances with 30k Users](https://docs.gitlab.com/ee/update/versions/gitlab_16_changes.html#long-running-user-type-data-change)"
          - major: 16
            minor: 7
        YML
      end

      before do
        stub_request(:get, "https://gitlab.com/api/v4/projects/20699/repository/tags?search=^16.7.")
          .to_return(status: 200, body: tags)

        stub_request(:get, "https://gitlab.com/gitlab-org/gitlab/-/raw/master/config/upgrade_path.yml")
          .to_return(status: 200, body: upgrade_path_yml)

        # rubocop:disable RSpec/SubjectStub
        allow(pg_dump_generator).to receive(:git_clone_gitlab)
        allow(pg_dump_generator).to receive(:create_pg_dump).and_return("#{Dir.pwd}/tmp/pd_dump/gitlabhq_production.gz")
        allow(pg_dump_generator).to receive(:run_command)
        allow(File).to receive(:file?).and_return(false)
        allow(FileUtils).to receive(:mkdir_p)
        allow(FileUtils).to receive(:cp)
      end

      it 'initializes with latest GitLab upgrade stop version' do
        expect(pg_dump_generator.instance_variable_get(:@gitlab_version)).to eq(Semantic::Version.new("16.7.5"))
      end

      it 'creates a pg dump and copies it with the correct name' do
        pg_dump_file_path = pg_dump_generator.create_pg_dump_gz_files

        expect(pg_dump_file_path).to eq("#{Dir.pwd}/pg_dumps/ee/16.7.5.gz")
      end
    end

    context 'when gitlab_version is not provided and last upgrade stop is not available' do
      subject(:pg_dump_generator) { described_class.new(options) }

      let(:options) { { gitlab_version: nil, gitlab_docker_image: 'gitlab/gitlab-ee', gitlab_repo_path: nil } }

      let(:tags) do
        <<~JSON
          [
            {"message": "", "name": "16.7.6+rc42.ee.0"},
            {"message": "", "name": "16.7.5+ee.0"},
            {"message": "", "name": "16.7.5+ce.0"},
            {"message": "", "name": "16.7.4+ee.0"},
            {"message": "", "name": "16.7.4+ce.0"}
          ]
        JSON
      end

      let(:upgrade_path_yml_with_unreleased_last_version) do
        <<~YML
          - major: 16
            minor: 3
          - major: 16
            minor: 7
          - major: 16
            minor: 11
          - major: 17
            minor: 3
        YML
      end

      before do
        stub_request(:get, "https://gitlab.com/api/v4/projects/20699/repository/tags?search=^17.3.")
          .to_return(status: 200, body: '[]')

        stub_request(:get, "https://gitlab.com/api/v4/projects/20699/repository/tags?search=^16.11.")
          .to_return(status: 200, body: '[]')

        stub_request(:get, "https://gitlab.com/api/v4/projects/20699/repository/tags?search=^16.7.")
          .to_return(status: 200, body: tags)

        stub_request(:get, "https://gitlab.com/gitlab-org/gitlab/-/raw/master/config/upgrade_path.yml")
          .to_return(status: 200, body: upgrade_path_yml_with_unreleased_last_version)
      end

      it 'initializes with the latest released GitLab upgrade stop version' do
        expect(pg_dump_generator.instance_variable_get(:@gitlab_version)).to eq(Semantic::Version.new("16.7.5"))
      end
    end
  end
end
