# PG Dump Generator

PG Dump Generator is used to generate and store PG dumps for upgrade testing.

[[_TOC_]]

## Overview

PG Dump Generator script follows the below workflow:

1. Identifies the latest upgrade stop version if no specific version is provided to the tool. For the latest upgrade stop, it pulls the latest version from [`upgrade_path.yml`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/upgrade_path.yml?ref_type=heads) and checks the latest patch version for this release from [`gitlab-omnibus` tags](https://gitlab.com/gitlab-org/omnibus-gitlab/-/tags).
1. Checks if PG dump file for the version already exists. If yes, script will exit unless forced with `force` option.
1. Prepares local GitLab repository if no custom path is provided with `--gitlab-repo-path`. Script shallow clones `master` branch from [GitLab](https://gitlab.com/gitlab-org/gitlab/) project to [`tmp`](./tmp/) folder. It also outputs the SHA version of the GitLab repo for tracking.
1. Pulls and starts GitLab docker image for identified version and mounts GitLab repository to it so that it can use [GitLab Data Seeder](https://docs.gitlab.com/ee/topics/data_seeder.html) configurations and [`db:seed_fu` rake](https://docs.gitlab.com/ee/development/development_seed_files.html) as well as prepare spec files and gems that are required for fixtures to run.
   - Detailed information: By default, GitLab Docker container doesn't have development gems and `spec` as it's not needed for customers. To bypass this, Generator needs to prepare these files to allow Data Seeder to run. To achieve this, the tool runs [`scripts/data_seeder/test_resources.sh`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/scripts/data_seeder/test_resources.sh?ref_type=heads) in GitLab Docker image that copies the `spec` and `ee/spec` directories from GitLab `ref` = upgrade stop to the `gitlab-rails` service directory within Docker. Then adds `Gemfile` and runs [`scripts/data_seeder/globalize_gems.rb`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/scripts/data_seeder/globalize_gems.rb?ref_type=heads) to prepare all required gems for Data Seeder and `FactoryBot`. This allows to always ensure that `FactoryBot` configuration is up-to-date with relevant configuration that was used for Factories in specific upgrade stop tag.
   - Once GitLab Data Seeder command finished, PG Dump Generator runs [`db:seed_fu` rake](https://docs.gitlab.com/ee/development/development_seed_files.html) command for to seed fixtures from [`db/fixtures`](https://gitlab.com/gitlab-org/gitlab/-/tree/master/db/fixtures?ref_type=heads). `db/fixtures` folder is copied as part of [`scripts/data_seeder/test_resources.sh`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/scripts/data_seeder/test_resources.sh?ref_type=heads). Tool runs each fixture file one by one from the `db/fixtures` of specific required stop to ensure that all fixtures are run against the image.
1. Creating PG dump file locally in [`tmp`](./tmp/) folder.
1. Copies PG dump file to [`pg_dumps`](./pg_dumps/) folder. If latest upgrade stop version is used, it will also copy file to `latest_upgrade_stop.gz` file, so that latest upgrade file is always rotated and up-to-date.
1. Optionally if `ACCESS_TOKEN` and `commit` flag are provided, tool commits and merges PG Dump after generation.
1. Optionally if `SLACK_BOT_TOKEN` and `--report-slack` are provided, generator posts message to Slack channel with results of PG dump generation.

## Prepare local environment

You can run the tool natively on a Unix based machine (Linux or Mac). Before running some setup is required for the PG Dump Generator tool specifically:

1. That [Git LFS](https://git-lfs.github.com/) is installed and any LFS data is confirmed pulled via `git lfs pull`.
1. [Docker](https://docs.docker.com/) is installed
1. First, set up [`Ruby`](https://www.ruby-lang.org/en/documentation/installation/) using Ruby version specified in [`.ruby_version` file](.ruby_version) and [`Ruby Bundler`](https://bundler.io) if they aren't already available on the machine.
1. Next, install the required Ruby Gems via Bundler
    - `bundle install`

## Running the PG Dump Generator

Once setup is done you can run the tool with the `bin/generate-pg-dump` script. The options for running the tests are the following:

```txt
Options:
  -g, --gitlab-version=<s>         The GitLab version to generate PG dump for. Must be in the format Major.Minor.Patch like 17.1.3
  -i, --gitlab-docker-image=<s>    GitLab docker image to use for generation. (Default: gitlab/gitlab-ee)
  -d, --gds-config-name=<s>        GitLab Data Seeder configuration name. (Default: bulk_data.rb)
  -t, --gitlab-repo-path=<s>       Full Path to the local GitLab repository to be mounted to GitLab container.
  -s, --seed-fu, --no-seed-fu      Seed GitLab docker image using Seed Fu and GitLab fixtures. (Default: true)
  -f, --force                      Force PG dump generation even if files exist.
  -c, --commit                     Commit and merge PG dump after generation. Requires 'ACCESS_TOKEN' environment variable.
  -h, --channel=<s>                Slack channel to post results to. (Default: tp-upgrade-results)
  -r, --report-slack               Report results to Slack channel. Requires 'SLACK_BOT_TOKEN' environment variable.
Generate PG dump for the latest upgrade stop:
    bin/generate-pg-dump
Generate PG dump for the latest upgrade stop using local GDK path:
    bin/generate-pg-dump --gitlab-repo-path '/Users/username/gitlab-development-kit/gitlab'
Generate PG dump for the specific GitLab version and Docker image:
    bin/generate-pg-dump --gitlab-version 16.3.6 --gitlab-docker-image gitlab/gitlab-ce
  -e, --help                       Show this message
```

### Running the PG Dump Generator using local GDK

PG Dump Generator can use local path to GitLab repository, for example [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit/) or local repo of [GitLab](https://gitlab.com/gitlab-org/gitlab/) project. In this case, full Path to the local GitLab repository will be mounted to GitLab container. Provide your custom full path using the following command:

```sh
bin/generate-pg-dump --gitlab-repo-path '/Users/username/gitlab-development-kit/gitlab'
```

This can be helpful when you want to create a custom PG dump file using custom Data Seeder configuration. To achieve that, ensure that your Data Seeder file is stored in `gitlab/ee/db/seeds/data_seeder/<gds_config_file>.rb` in your local GitLab repo.

```sh
bin/generate-pg-dump --gitlab-repo-path '/Users/username/gitlab-development-kit/gitlab' --gds-config-name='<gds_config_file>.rb'
```

## PG dump files

PG dump files that were generated with the tool can be found in [`pg_dumps`](./pg_dumps/) folder. These files are used for multi-version migration testing in GitLab CI and can be used for local testing.

Files are stored as `gz` extension files using Git LFS. To use files locally, ensure to run `git lfs pull`.

By default, the tool uses [`bulk_data.rb`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/db/seeds/data_seeder/bulk_data.rb?ref_type=heads) Data Seeder configuration file to seed the environments. The file is being kept up-to-date with fixture seeding. To check which GitLab `master` SHA version of the file was used navigate to MR that added the file and check SHA in the job logs `Outputting HEAD SHA of the GitLab path`.

New PG dump files are generated with [scheduled pipelines](https://gitlab.com/gitlab-org/quality/pg-dump-generator/-/pipeline_schedules).

## Migration upgrade testing

PG dump files can be used for local multi-version migration testing. Depending on your local setup you can run the test against GitLab Docker instance or GitLab Development Kit (GDK).

:warning: Note that the process will involve dropping the database and importing the PG dump.

### GitLab Development Kit - GDK

Multi-version migration test can be run against GDK, however it will require to drop and recreate database. As such, you might need to run [GDK pristine](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/gdk_commands.md#gdk-pristine) to restore GDK back after testing.

Guidance how to run multi-version migration testing against GDK:

1. Select the PG file to use. By default, you can use `latest_upgrade_stop.gz` file to check migrations against the latest stop. You can also select older files from specific GitLab version if needed.
1. In GDK navigate to `gitlab` folder and pull the latest upgrade stop file.

   ```sh
   curl -o latest_upgrade_stop.gz https://gitlab.com/gitlab-org/quality/pg-dump-generator/-/raw/main/pg_dumps/ee/latest_upgrade_stop.gz
   ```

1. Unzip the file

   ```sh
   gunzip -c latest_upgrade_stop.gz > latest_upgrade_stop
   ```

1. Edit `gitlabhq_production` file - update all `OWNER TO gitlab` to `OWNER TO X` and `Owner: gitlab` to `Owner: X` where X is your PG owner in GDK DB - output of `whoami` command

   ```sh
   export USERNAME=$(whoami)
   sed -e "s/OWNER TO gitlab/OWNER TO $USERNAME/g" -e "s/Owner: gitlab/Owner: $USERNAME/g" latest_upgrade_stop > gitlabhq_production
   ```

1. In GDK GitLab console, stop all DB clients:

   ```ruby
   gdk stop; gdk start postgresql; gdk start redis
   ```

1. Drop and recreate database

   ```ruby
   bundle exec rake db:drop db:create
   ```

1. Import DB dump using `psql`, where username is the name of your user in GDK (`whoami` output)

   ```sh
   gdk psql -U $USERNAME -d gitlabhq_development < gitlabhq_production
   ```

1. In run reconfigure to trigger migrations

   ```ruby
   bundle exec rake gitlab:db:configure
   ```

### GitLab Docker

Guidance how to run multi-version migration testing against GitLab Docker container:

1. Start [GitLab docker container](https://docs.gitlab.com/ee/install/docker.html)
1. Select the PG file to use. By default, you can use `latest_upgrade_stop.gz` file to check migrations against the latest stop. You can also select older files from specific GitLab version if needed.
1. Pull the latest upgrade stop file.

   ```sh
   curl -o latest_upgrade_stop.gz https://gitlab.com/gitlab-org/quality/pg-dump-generator/-/raw/main/pg_dumps/ee/latest_upgrade_stop.gz
   ```

1. Unzip the file

   ```sh
   gunzip -c latest_upgrade_stop.gz > latest_upgrade_stop
   ```

1. Copy `latest_upgrade_stop` to GitLab container

   ```sh
   docker cp latest_upgrade_stop container_id:/latest_upgrade_stop
   ```

1. Exec to GitLab container

   ```sh
   docker exec -it container_id /bin/bash
   ```

1. Stop all DB clients:

   ```sh
   gitlab-ctl stop; gitlab-ctl start postgresql; gitlab-ctl start redis
   ```

1. Drop database

   ```ruby
   DISABLE_DATABASE_ENVIRONMENT_CHECK=1 gitlab-rake db:drop
   ```

1. Exec to GitLab container as `gitlab-psql` user and recreate database

   ```shell
   docker exec --user gitlab-psql -it gitlab_master /bin/bash
   /opt/gitlab/embedded/bin/psql -h /var/opt/gitlab/postgresql -d template1 -c "CREATE DATABASE gitlabhq_production OWNER gitlab;"
   ```

1. Exec to GitLab container

   ```sh
   docker exec -it container_id /bin/bash
   ```

1. Import DB dump using `gitlab-psql`

   ```sh
   gitlab-psql -d gitlabhq_production < latest_upgrade_stop
   ```

1. Run reconfigure to trigger migrations

   ```ruby
   gitlab-rake gitlab:db:configure
   ```
